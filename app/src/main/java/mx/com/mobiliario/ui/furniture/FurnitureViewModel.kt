package mx.com.mobiliario.ui.furniture

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import mx.com.mobiliario.ui.furniture.model.Furniture

class FurnitureViewModel : ViewModel() {

    private val _furnitures = MutableLiveData<MutableList<Furniture>>()
    val furnitures: LiveData<MutableList<Furniture>>
        get() = _furnitures

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    init {
        getFurnitures()
    }

    private fun getFurnitures() {
        _loading.postValue(true)
        val db = FirebaseFirestore.getInstance()
        db.collection("furnitures")
                .get()
                .addOnSuccessListener { result ->
                    val furnituresList =  mutableListOf<Furniture>()
                    for (document in result) {
                        val image = document.data["image"] as String
                        val name = document.data["name"] as String
                        val price = document.data["price"] as String
                        val color = document.data["color"] as String
                        furnituresList.add(Furniture(name = name, image = image, price = price, color = color))
                    }
                    _loading.postValue(false)
                    _furnitures.postValue(furnituresList)
                }
                .addOnFailureListener { exception ->
                    _loading.postValue(false)
                    Log.w("TAG", "Error getting documents.", exception)
                }
    }
}