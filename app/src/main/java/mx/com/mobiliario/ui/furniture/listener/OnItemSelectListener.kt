package mx.com.mobiliario.ui.furniture.listener

import mx.com.mobiliario.databinding.ItemFurnitureBinding

interface OnItemSelectListener {
    fun selectItem(binding: ItemFurnitureBinding, item: Any)
}