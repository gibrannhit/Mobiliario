package mx.com.mobiliario.ui.details

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide.with
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.transition.platform.MaterialArcMotion
import com.google.android.material.transition.platform.MaterialContainerTransform
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_furniture_fragment.*
import mx.com.mobiliario.R
import mx.com.mobiliario.ui.furniture.model.Furniture
import mx.com.mobiliario.util.extensions.startEnterTransitionAfterLoadingImage

class DetailFurnitureFragment : Fragment() {

    private lateinit var viewModel: DetailFurnitureViewModel
    private var furniture: Furniture? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_furniture_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            furniture = it.getParcelable("furniture")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSharedElementTransitionOnEnter()
        setUpData()
    }

    private fun setUpData() {
        furniture?.let { item ->
            ivImageFurnitureDetail.apply {
                transitionName = item.image
                startEnterTransitionAfterLoadingImage(item.image, this)
            }
            clContainerDetail.setBackgroundColor(Color.parseColor(item.color))
            tvTitleFurniture.text = item.name
            val price = "$${item.price}"
            tvPriceFurniture.text = price
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailFurnitureViewModel::class.java)
    }

    private fun setSharedElementTransitionOnEnter() {
        sharedElementEnterTransition = MaterialContainerTransform().apply {
            pathMotion = MaterialArcMotion()
            isElevationShadowEnabled = true
            scrimColor = Color.TRANSPARENT
            fadeMode = MaterialContainerTransform.FADE_MODE_CROSS
        }
    }
}