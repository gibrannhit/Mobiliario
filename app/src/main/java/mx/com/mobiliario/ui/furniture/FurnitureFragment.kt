package mx.com.mobiliario.ui.furniture

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.google.android.material.transition.platform.MaterialElevationScale
import kotlinx.android.synthetic.main.furniture_fragment.*
import mx.com.mobiliario.R
import mx.com.mobiliario.ui.furniture.adapter.FurnitureAdapter
import mx.com.mobiliario.ui.furniture.model.Furniture
import mx.com.mobiliario.util.extensions.configProgressBar
import mx.com.mobiliario.util.extensions.observe

class FurnitureFragment : Fragment() {

    private lateinit var viewModel: FurnitureViewModel

    private val adapter: FurnitureAdapter by lazy { setAdapter() }

    private val dialog: Dialog by lazy { configProgressBar(R.color.purple_200) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.furniture_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(FurnitureViewModel::class.java)
        setUpRvFurniture()
        observeViewModel()
        setExitToFullScreenTransition()
        setReturnFromFullScreenTransition()
    }

    private fun observeViewModel() {
        observe(viewModel.loading, ::loading)
        observe(viewModel.furnitures, ::setList)
    }

    private fun selectItem(view: View, item: Furniture) {
        val extras = FragmentNavigatorExtras(view to item.image)
        findNavController().navigate(R.id.action_furnitureFragment_to_detailFurnitureFragment, Bundle().apply {
            putParcelable("furniture", item)
        }, null, extras)
    }

    private fun setExitToFullScreenTransition() {
        exitTransition = MaterialElevationScale(/* growing= */ false)
    }

    private fun setReturnFromFullScreenTransition() {
        reenterTransition = MaterialElevationScale(/* growing= */ true)
    }

    private fun setAdapter() = FurnitureAdapter(requireContext()) { image, furniture ->
        selectItem(image, furniture)
    }

    private fun setUpRvFurniture() {
        rvFurniture.run {
            adapter = this@FurnitureFragment.adapter
            setHasFixedSize(true)
            postponeEnterTransition()
            viewTreeObserver.addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
        }
    }

    private fun setList(list: MutableList<Furniture>){
        adapter.addList(list)
    }

    private fun loading(loaded: Boolean) {
        if (loaded) dialog.show() else dialog.dismiss()
    }

}