package mx.com.mobiliario.ui.furniture.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import mx.com.mobiliario.R
import mx.com.mobiliario.databinding.ItemFurnitureBinding
import mx.com.mobiliario.ui.furniture.model.Furniture
import mx.com.mobiliario.util.load

class FurnitureAdapter(private val context: Context, private val listener: ((image: View, furniture: Furniture) -> Unit)? = null) :
    RecyclerView.Adapter<FurnitureAdapter.FurnitureViewHolder>(){

    private var furnitures: MutableList<Furniture> =  mutableListOf()

    private var pos: Int = 0

    fun addList(furnitures: MutableList<Furniture>) {
        this.furnitures = furnitures
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FurnitureViewHolder =
        FurnitureViewHolder(ItemFurnitureBinding.inflate(LayoutInflater.from(parent.context)))

    override fun getItemCount(): Int = furnitures.size

    override fun onBindViewHolder(holder: FurnitureAdapter.FurnitureViewHolder, position: Int) {
        val furniture = furnitures[position]
        holder.binding.furniture = furniture
        holder.bind(holder.binding, furniture, listener)
        holder.binding.llContainer.setCardBackgroundColor(Color.parseColor(furniture.color))
        holder.binding.executePendingBindings()
    }

    inner class FurnitureViewHolder(val binding: ItemFurnitureBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(binding: ItemFurnitureBinding, item: Furniture, onDoggoClickListener: ((image: View, item: Furniture) -> Unit)?) {
            with(binding.ivImageFurniture) {
                transitionName = item.image
            }
            binding.root.setOnClickListener {
                onDoggoClickListener?.let {
                    it(binding.ivImageFurniture, item)
                }
            }

        }
    }

}