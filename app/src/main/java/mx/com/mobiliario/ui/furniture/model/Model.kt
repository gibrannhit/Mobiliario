package mx.com.mobiliario.ui.furniture.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Furniture(val name: String,
                     val image: String,
                     val price: String,
                     val color: String): Parcelable