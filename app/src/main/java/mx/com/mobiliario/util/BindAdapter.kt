package mx.com.mobiliario.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter(value = ["setImageUrl"])
fun bindImageUrl(image: ImageView, url: String?) {
    url?.let {
        Picasso.get().load(it).into(image)
    }
}
